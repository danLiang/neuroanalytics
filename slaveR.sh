#!/bin/bash
#SBATCH -p steinlab
#SBATCH -N 1
#SBATCH -t 30:00
#SBATCH --mem=4g
#SBATCH -n 1
#SBATCH -o slaveR.out

module add r/3.6.0;
file=$1;
R --file=$file  --slave --args $* ;
#Rscript $file $*;
exit 0

