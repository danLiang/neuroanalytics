#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -t 30:00
#SBATCH --mem=4g
#SBATCH -n 2
#SBATCH -o submit_hellos.out

sbatch ./sayworld.sh;
sbatch ./helloworld.sh;

