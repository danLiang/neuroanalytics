#!/bin/bash

#SBATCH -p steinlab
#SBATCH -N 1
#SBATCH -t 10:00
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -o plotAcircle.out

##submit jobs to plot a circle

sbatch slaveR.sh plotAcircle_withCustomR.R 3;
sbatch slaveR.sh plotAcircle_withCustomR.R 5; 



