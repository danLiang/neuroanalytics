#!/bin/bash

#SBATCH -p steinlab
#SBATCH -N 1
#SBATCH -t 10:00
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -o plotAcircle.out

##submit a R job to plot a circle
module add r/3.6.0;
Rscript plotAcircle.R;


##instead of using this code
## you can also use the command 
## module add r/3.6.0;
## sbatch -N 1 -n 1 --time=10:00 --mem=4g -p steinlab -o plotAcircle.out --wrap="Rscript plotAcircle.R"


